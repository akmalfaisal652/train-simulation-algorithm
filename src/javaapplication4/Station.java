/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;

/**
 *
 * @author Akmal Faisal
 */
import java.util.*;
import java.util.Queue;
import java.util.ArrayDeque;

public class Station {

    private Queue waiting;
    static int timeToNextStation;
    static int stationNumber;
    private java.util.Random random;

    /**
     * Constructor for objects of class Station
     */
    public Station(int stationNumber, int timeToNext) {
        this.waiting = new ArrayDeque<>();
        this.timeToNextStation = timeToNext;
        this.random = new ExponentialRandom(timeToNextStation);
        this.stationNumber = stationNumber;
    }

    public void addPassenger(Passenger passenger) {
        this.waiting.offer(passenger);
    }

    public boolean isWaiting() {
        return !this.waiting.isEmpty();
    }

    public Passenger getPassenger() {
        return (Passenger) this.waiting.poll();
    }

    public int getTimeToNextStation() {
        return this.timeToNextStation;
    }

    public void StationPassengers() {
        System.out.println(this.waiting.contains(waiting));
    }

    public String toString() {
        return "Station " + this.stationNumber + " has " + (isWaiting() ? "some" : "no")
                + " passengers waiting; the time to next station is "
                + this.timeToNextStation;
    }

}
