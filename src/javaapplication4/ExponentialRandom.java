/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;

/**
 *
 * @author Akmal Faisal
 */
public class ExponentialRandom extends java.util.Random {
  private double mean;

  public ExponentialRandom(double mean) {
    super(System.currentTimeMillis());
    this.mean = mean;
  }

  public double nextDouble() {
    return -mean*Math.log(1.0-super.nextDouble());
  }

  public int nextInt() {
    return (int)Math.ceil(nextDouble());
  }    
}

