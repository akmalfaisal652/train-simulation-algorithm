/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;

/**
 *
 * @author Akmal Faisal
 */
public class Passenger {

    private int startedWaiting;
    private int boardedAt;
    private boolean boardedTrain;
    private int destination;
    private int startStation;

    Passenger(int startStation, int destination, int createdAt) {
        this.startedWaiting = createdAt;
        this.startStation = startStation;
        this.destination = destination;
        this.boardedTrain = false;
    }

    public int getDestination() {
        return this.destination;
    }

    public void boardTrain(int clock) {
        this.boardedAt = clock;
        this.boardedTrain = true;
    }

    public int waitTime(int clock) {
        int result = clock - this.startedWaiting;
        if (this.boardedTrain) {
            result = this.boardedAt - this.startedWaiting;
        }

        return result;
    }

    public boolean boarded() {
        return this.boardedTrain;
    }

    public String toString() {
        return "Passenger arrived at time marker " + this.startedWaiting + " at station " + this.startStation + " heading to " + this.destination;
    }
}
